package com.nespresso.sofa.interview.parking;

/**
 * @author Hieu Pham (hieu.pham@domo-safety.com)
 */
public enum BayStatus {
    occupied, empty
}
