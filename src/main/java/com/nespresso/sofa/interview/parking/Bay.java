package com.nespresso.sofa.interview.parking;

/**
 * @author Hieu Pham (hieu.pham@domo-safety.com)
 */
public class Bay {
    private BayStatus bayStatus;
    private BayType bayType;
    private Character carType;

    public Bay(BayType bayType) {
        this.bayType = bayType;
        this.bayStatus = BayStatus.empty;
    }

    public boolean isAvailable(boolean isForDisabled){
        if (isForDisabled){
            return bayType.equals(BayType.disabled_only) && bayStatus.equals(BayStatus.empty);
        }
        else{
            return bayType.equals(BayType.non_disabled) && bayStatus.equals(BayStatus.empty);
        }
    }

    public BayStatus getBayStatus() {
        return bayStatus;
    }

    public void setBayStatus(BayStatus bayStatus) {
        this.bayStatus = bayStatus;
    }

    public BayType getBayType() {
        return bayType;
    }

    public void setBayType(BayType bayType) {
        this.bayType = bayType;
    }

    public void setCarType(Character carType) {
        this.carType = carType;
    }

    @Override
    public String toString() {
        if (carType != null){
            return String.valueOf(carType);
        }
        else if (bayType.equals(BayType.pedestrian_exit)){
            return "=";
        }
        else if (bayType.equals(BayType.disabled_only)){
            if (bayStatus.equals(BayStatus.empty)){
                return "@";
            }
            else {
                return "D";
            }
        }
        else {
            //this case the bay is empty, because if it is not empty, there must be a carType
            return "U";
        }
    }
}
