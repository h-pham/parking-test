package com.nespresso.sofa.interview.parking;

import java.util.ArrayList;
import java.util.List;

/**
 * Builder class to get a parking instance
 */
public class ParkingBuilder {

    private int size;
    private List<Integer> pedestrianExitIndexes;
    private List<Integer> disabledBayIndexes;

    public ParkingBuilder() {
        this.pedestrianExitIndexes = new ArrayList<>();
        this.disabledBayIndexes = new ArrayList<>();
    }

    /**
     *
     * @param size
     * @return
     */
    public ParkingBuilder withSquareSize(final int size) {
        this.size = size;
        return this;
    }

    public ParkingBuilder withPedestrianExit(final int pedestrianExitIndex) {
        if (!this.pedestrianExitIndexes.contains(pedestrianExitIndex)){
            this.pedestrianExitIndexes.add(pedestrianExitIndex);
        }
        return this;
    }

    public ParkingBuilder withDisabledBay(final int disabledBayIndex) {
        if (!this.disabledBayIndexes.contains(disabledBayIndex)){
            this.disabledBayIndexes.add(disabledBayIndex);
        }
        return this;
    }

    public Parking build() {
        return new Parking(size, pedestrianExitIndexes, disabledBayIndexes);
    }
}
