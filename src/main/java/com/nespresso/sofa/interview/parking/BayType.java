package com.nespresso.sofa.interview.parking;

/**
 * @author Hieu Pham (hieu.pham@domo-safety.com)
 */
public enum BayType {
    //pedestrian exit
    pedestrian_exit,
    // bay for disabled only
    disabled_only,
    // bay for non disabled
    non_disabled
}
