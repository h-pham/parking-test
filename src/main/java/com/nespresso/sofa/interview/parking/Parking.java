package com.nespresso.sofa.interview.parking;

import java.util.ArrayList;
import java.util.List;

/**
 * Handles the parking mechanisms: park/unpark a car (also for disabled-only bays) and provides a string representation of its state.
 */
public class Parking {

    private final int capacity;
    private final int size;
    private final List<Integer> pedestrianExitIndexes;
    private final List<Integer> disabledBayIndexes;
    // a list of bay in order of a travel path of a car that enters the parking and go until the end, doing U turn at the end of each lane
    private List<Bay> parkingSlots;

    public Parking(int size, List<Integer> pedestrianExitIndexes, List<Integer> disabledBayIndexes) {
        this.size = size;
        this.capacity = size*size;
        this.pedestrianExitIndexes = pedestrianExitIndexes;
        this.disabledBayIndexes = disabledBayIndexes;
        initialSetup();
    }

    void initialSetup() {
        this.parkingSlots = new ArrayList();
        //firstly add new bay in default type non_disabled to the parking
        for (int i = 0; i< capacity; i++){
            parkingSlots.add(new Bay(BayType.non_disabled));
        }
        // correct the pedestrian exit type
        for (Integer idx : pedestrianExitIndexes){
            parkingSlots.get(idx).setBayType(BayType.pedestrian_exit);
        }
        // correct the disabled only type
        for (Integer idx: disabledBayIndexes){
            parkingSlots.get(idx).setBayType(BayType.disabled_only);
        }
    }

    /**
     * @return the number of available parking bays left
     */
    public int getAvailableBays() {
        return ((int) parkingSlots.stream()
                .filter(bay -> bay.getBayType() != BayType.pedestrian_exit && bay.getBayStatus().equals(BayStatus.empty))
                .count());
    }

    /**
     * Park the car of the given type ('D' being dedicated to disabled people) in closest -to pedestrian pedestrian_exit- and first (starting from the parking's entrance)
     * available bay. Disabled people can only park on dedicated bays.
     *
     *
     * @param carType
     *            the car char representation that has to be parked
     * @return bay index of the parked car, -1 if no applicable bay found
     */
    public int parkCar(final char carType) {
        //we loop through the parkingSlots using travelPath indexes
        int candidateBayIdx = -1;
        int distanceToExit = capacity;
        boolean isForDisabled = carType == 'D';
        for (int i = 0; i < parkingSlots.size(); i++){
            Bay bay = parkingSlots.get(i);
            if (!bay.isAvailable(isForDisabled)){
                continue;
            }
            int minDistance = findMinDistanceToPedestrianExits(i);
            if (candidateBayIdx == -1 || minDistance < distanceToExit){
                candidateBayIdx = i;
                distanceToExit = minDistance;
            }
            //because of the nature of loop from index 0, the second condition of nearest to the entrance is satisfied.
        }
        if (candidateBayIdx!=-1){
            Bay candidate = parkingSlots.get(candidateBayIdx);
            candidate.setCarType(carType);
            candidate.setBayStatus(BayStatus.occupied);
        }
        return candidateBayIdx;
    }

    /**
     * Unpark the car from the given index
     *
     * @param index
     * @return true if a car was parked in the bay, false otherwise
     */
    public boolean unparkCar(final int index) {
        Bay bay = parkingSlots.get(index);
        // if the bay is pedestrian exit, there's no car parking there
        // else check if bay is empty or not
        if (bay.getBayStatus().equals(BayStatus.occupied)){
            bay.setCarType(null);
            bay.setBayStatus(BayStatus.empty);
            return true;
        }
        return false;
    }

    /**
     * Print a 2-dimensional representation of the parking with the following rules:
     * <ul>
     * <li>'=' is a pedestrian pedestrian_exit
     * <li>'@' is a disabled-only empty bay
     * <li>'U' is a non-disabled empty bay
     * <li>'D' is a disabled-only occupied bay
     * <li>the char representation of a parked vehicle for non-empty bays.
     * </ul>
     * U, D, @ and = can be considered as reserved chars.
     *
     * Once an end of lane is reached, then the next lane is reversed (to represent the fact that cars need to turn around)
     *
     * @return the string representation of the parking as a 2-dimensional square. Note that cars do a U turn to continue to the next lane.
     */

    int findMinDistanceToPedestrianExits(int currentIndex){
        //distance is according to the travel path
        int minDistance = capacity;
        for (Integer exitIndex : pedestrianExitIndexes){
            int distance = Math.abs(exitIndex - currentIndex);
            if (distance <minDistance){
                minDistance = distance;
            }
        }
        return minDistance;
    }

    int convertToTravelPathIndex(int index){
        //car will go in reversed direction on odd lane (zero-indexed)
        int laneIndex = index/size;
        int laneOffset = index%size;
        boolean is_reversing = laneIndex%2!=0;
        if (is_reversing){
            return (laneIndex+1)*size-laneOffset-1;
        }
        else{
            return index;
        }
    }

    @Override
    public String toString() {
        List<Integer> travelPathIndexes = new ArrayList();
        for (int i = 0; i< size; i++){
            boolean is_reversing = i%2 != 0;
            for (int j = 0; j< size; j++){
                // entrance is at index 0; lane 0;
                // car only going on the reverse direction on odd lane
                if (is_reversing){
                    travelPathIndexes.add((i+1)* size -j-1);
                }
                else {
                    travelPathIndexes.add(i* size +j);
                }
            }
        }
//        String representation = travelPathIndexes.stream().map(i -> parkingSlots.get(i).toString()).collect(Collectors.joining(""));
        String representation = "";
        for (int i = 0; i<travelPathIndexes.size(); i++){
            //can use StringBuilder for better performance
            if (i%size == 0 && i>0){
                representation += "\n";
            }
            representation += parkingSlots.get(travelPathIndexes.get(i)).toString();
        }
        return representation;
    }

}
